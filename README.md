# Fatto fin ora

Creati i vari modelli (Client, User, Project, Activity...) e corrispettive migration table (cartella -> 'App\').
Predisposti i file di Control per i rispettivi modelli e implementato con funzioanlità CRUD (cartella -> 'App\Http\Controllers'.
Gesiste diverse view tramite anche i template blade.php e implementate quelle necessarie per i CLients ( index, show, create, update, destroy ) ('resources\view').
Gestiti problemi del gestore pacchetti NPM.

# TODO

  - Implementare CRUD sul resto dei modelli che lo necessitano
  - Implementare upload immagini/video
  - Sistemare i layout e frontend, riducendo anche il riutilizzo di codice
  - Creare UnitTest
  - Implementazione script JQuary
  - Commentare il più possibile il PRIMA possibile


## Tecnologie usate

Sono stati usati vagrant/homestead per l'ambiente virtuale.
Il CSS utilizzato è basato su Bootstrap