<?php

namespace App\Http\Controllers;

use App\User;
use App\Client;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use View;
use Auth;
use Validator;
use Session;

class ClientController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        if (Auth::check()){
            $clients = User::with('clients')->find(Auth::user()->id)->clients;//Client::where('worker_id', Auth::user()->id);
        }else{
            $clients = [];
        }
        return View::make('client.index')
                ->with('clients', $clients);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return View::make('client.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        // validate
        // read more on validation at http://laravel.com/docs/validation
        $rules = array(
            'name'       => 'required',
            'surname'    => 'required',
            'email'      => 'required|email',
            'phone_1'    => 'nullable',
            'phone_2'    => 'nullable',
            'worker_id'  => 'required|numeric'
        );
        $validator = Validator::make(Input::all(), $rules);

        // process the login
        if ($validator->fails() || !Auth::check()) {
            return Redirect::to('client/create')
                ->withErrors($validator)
                ->withInput(Input::except('password'));
        } else {
            // store
            $client = new Client;
            $client->name       = Input::get('name');
            $client->surname    = Input::get('surname');
            $client->email      = Input::get('email');
            $client->phone_n1    = Input::get('phone_1');
            $client->phone_n2    = Input::get('phone_2');
            $client->worker_id  = Input::get('worker_id');
            if (Auth::user()->id != $client->worker_id){
                Session::flash('message', 'Non puoi registrare un utente come fossi qualcun altro!');
                return Redirect::to('client');
            }
            $client->save();

            // redirect
            Session::flash('message', 'Cliente registrato con successo');
            return Redirect::to('client');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Client  $client
     * @return \Illuminate\Http\Response
     */
    public function show(Client $client)
    {
        // get the client
        $client = Client::find($client->id);

        // show the view and pass the client to it
        return View::make('client.show')
            ->with('client', $client);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Client  $client
     * @return \Illuminate\Http\Response
     */
    public function edit(Client $client)
    {
        
        // get the client
        $client = Client::find($client->id);

        return View::make('client.edit')
            ->with('client', $client);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Client  $client
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Client $client)
    {
        $rules = array(
            'name'       => 'required',
            'surname'    => 'required',
            'email'      => 'required|email',
            'phone_1'    => 'nullable',
            'phone_2'    => 'nullable'
        );
        $validator = Validator::make(Input::all(), $rules);

        // process the login
        if ($validator->fails() || !Auth::check()) {
            return Redirect::to('client/' . $client->id . '/edit')
                ->withErrors($validator)
                ->withInput(Input::except('password'));
        } else {
            // store
            $client = Client::find($client->id);
            $client->name       = Input::get('name');
            $client->surname    = Input::get('surname');
            $client->email      = Input::get('email');
            $client->phone_n1    = Input::get('phone_1');
            $client->phone_n2    = Input::get('phone_2');
            if (Auth::user()->id != $client->worker_id){
                Session::flash('message', 'Non puoi modificare un cliente non tuo!');
                return Redirect::to('client/' . $client->id . '/edit');
            }
            $client->save();

            // redirect
            Session::flash('message', 'Cliente modificato con successo');
            return Redirect::to('client');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Client  $client
     * @return \Illuminate\Http\Response
     */
    public function destroy(Client $client)
    {
        // delete
        $client = client::find($client->id);
        $client->delete();

        // redirect
        Session::flash('message', 'Successfully deleted the client!');
        return Redirect::to('client');
    }
}
