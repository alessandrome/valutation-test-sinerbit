<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Project extends Model
{
    //
	public function userCreator(){
		return $this->belongsTo('App\User', 'user_creator_id');
	}
    
    public function userPartecipations(){
        return $this->belongsToMany('App\User', 'user_partecipations');
    }
    
    public function acitivities(){
        return $this->hasMany('App\Activity', 'project_id');
    }
    
    public function client(){
        return $this->hasOne('App\Client');
    }
}
