<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Media extends Model
{
    //
    public function delete()
    {
        if(file_exists('file_path')){
            unlink('file_path');
        }
        parent::delete();
    }
}
