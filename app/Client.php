<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Client extends Model
{
    //
    public function workerUsers(){
        return $this->belongsTo('App\User','client_user');
    }
    
    public function projects(){
        return $this->hasMany('App\Project');
    }
    
}
