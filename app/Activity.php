<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Activity extends Model
{
    //
    public function project(){
        return $table->belongsTo('App\Project');
    }
    
    public function userCreator(){
		return $this->belongsTo('App\User', 'foreign_key');
	}
    
    public function media(){
        return $this->hasOne('App\Media');
    }
}
