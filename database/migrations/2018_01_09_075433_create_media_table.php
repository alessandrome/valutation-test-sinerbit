<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMediaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('media', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('user_upload_id')->nullable(true);
            $table->string('path'); //storage/app/public
            $table->string('name');
            $table->enum('type', ['img','video']);
            $table->enum('ext',['jpg','png','avi']);
            $table->timestamps();
            
            //PK
        });
        
        //DB::statement('ALTER TABLE media ADD CONSTRAINT chk_type CHECK (type IN (\'img\', \'video\'));');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement("SET FOREIGN_KEY_CHECKS=0");
        Schema::dropIfExists('media');
        DB::statement("SET FOREIGN_KEY_CHECKS=1");
    }
}
