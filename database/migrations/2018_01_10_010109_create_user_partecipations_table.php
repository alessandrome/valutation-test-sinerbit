<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Carbon\Carbon;

class CreateUserPartecipationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_partecipations', function (Blueprint $table) {
            //$table->increments('id');
            $table->unsignedInteger('user_id');
            $table->unsignedInteger('project_id');
            $table->dateTime('join_dt')->default(Carbon::now()); //date("Y-m-d H:i:s") is the alternative PHP code for default dt
            $table->timestamps();
            
            //Define PK/FK, Indexes, ...
            $table->primary(['user_id', 'project_id']);
            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('project_id')->references('id')->on('projects');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement("SET FOREIGN_KEY_CHECKS=0");
        Schema::dropIfExists('user_partecipations');
        DB::statement("SET FOREIGN_KEY_CHECKS=1");
    }
}
