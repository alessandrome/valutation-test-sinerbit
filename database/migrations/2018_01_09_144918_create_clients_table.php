<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClientsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('clients', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('surname');
            $table->unsignedInteger('user_id')->nullable(true);
            $table->string('email');
            $table->string('phone_n1')->nullable(true);
            $table->string('phone_n2')->nullable(true);
            $table->unsignedInteger('worker_id');
            $table->timestamps();
            
            //Set foreign keys
            
        });
        
         Schema::table('clients', function($table) {
            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('worker_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement("SET FOREIGN_KEY_CHECKS=0");
        Schema::dropIfExists('clients');
        DB::statement("SET FOREIGN_KEY_CHECKS=1");
    }
}
