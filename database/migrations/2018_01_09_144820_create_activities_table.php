<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateActivitiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('activities', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('project_id');
            $table->unsignedInteger('user_creator_id');
            $table->dateTime('pubblication_dt');
            $table->enum('type', ['facebook','instagram','mybusiness','google']);
            $table->string('category');
            $table->string('text')->default('');
            $table->unsignedInteger('media_id')->nullable(true);
            $table->enum('validation_text', ['attesa','pubblicabile','revisionare'])->default('attesa');
            $table->enum('validation_media', ['attesa','pubblicabile','revisionare'])->default('attesa');
            $table->timestamps();
            
            //Define PK/FK, Indexes, ...
            $table->foreign('project_id')->references('id')->on('projects')->onDelete('cascade');
            $table->foreign('user_creator_id')->references('id')->on('users');
            $table->foreign('media_id')->references('id')->on('media');
            
            }
        );
        //Adding CONSTRAINT checks
        //DB::statement('ALTER TABLE activities ADD CONSTRAINT chk_validation CHECK (validation_text IN (\'attesa\', \'pubblicabile\', \'revisionare\') and validation_media IN (\'attesa\', \'pubblicabile\', \'revisionare\')),
        //                    CONSTRAINT chk_type CHECK type IN (\'facebook\', \'instagram\', \'mybusiness\', \'google\');');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement("SET FOREIGN_KEY_CHECKS=0");
        Schema::dropIfExists('activities');
        DB::statement("SET FOREIGN_KEY_CHECKS=1");
    }
}
