<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProjectsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('projects', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('user_creator_id')->nullable(true);
            $table->string('name');
            $table->string('description')->default('');
            $table->date("start_date");
            $table->date("end_date");
            $table->timestamps();
            
            //Define PK/FK, Indexes, ...
            $table->foreign('user_creator_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement("SET FOREIGN_KEY_CHECKS=0");
        Schema::dropIfExists('projects');
        DB::statement("SET FOREIGN_KEY_CHECKS=1");
    }
}
