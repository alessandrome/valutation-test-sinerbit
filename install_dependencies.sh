#!/usr/bin/env bash

composer install
cp .env.example .env
php artisan key:generate
npm install --no-bin-links
php artisan migrate