@extends('layouts.base')

@section('content')
<div class="container">
    @foreach ($projects as $key=>$value)
        <a href="{{ URL::to('project/'.$value->id) }}">
            <div class="card col-md-10 col-offset-1">
                <h4>{{ $value->name }}</h4>
                <div class="row">Start:{{ $value->start_date }} End:{{ $value->end_date }}</div>
                <div class="container">{{ $value->description }}</div>
            </div>
        </a>
    @endforeach
    <a href="{{ URL::to('project/create') }}" class="btn btn-link" >Create project</button>
    <a href="{{ URL::to('project') }}" class="btn btn-link" >View projects</button>
</div>
@endsection
