@extends('layouts.base')

@section('content')
<div class="container">

    <!-- if there are creation errors, they will show here -->
    <!-- HTML::ul($errors->all()) not more supported in laravel -->
    @section('errors')
    @if(isset($errors))
        <ul>
        @foreach($errors->all() as $error)
            <li>{{ $error }}</li>
        @endforeach
        </ul>
    @endif
    @endsection
    
    @guest
        <div class="panel panel-danger">
          <div class="panel-heading">Solo per utenti registrati!</div>
          <div class="panel-body">Se vuoi creare un progetto prima fai il login/registraizone e registra un cliente
            <br/>
            <a href="{{ URL::to('login') }}" class="card btn btn-link">Login</a> <a href="{{ URL::to('register') }}" class="card btn btn-link">Register</a>
          </div>
        </div>
    
    @else
        <form method="POST" action="/project">
            {{ csrf_field() }}
            <div class="form-group">
                <label name="name_label">Nome</label>
                <input type="text" name="name"></input> 
            </div>

            <div class="form-group">
                <label name="start_date_label">Inizio</label>
                <input type="date" name="start_date"></input> 
                
                <label name="end_date_label">Fine</label>
                <input type="date" name="end_date"></input> 
            </div>
            
            <div class="form-group">
                <label name="description_label">Descrizione</label>
                <input type="textbox" name="description"></input> 
            </div>
            <input type="submit"/>

            <script>
                $('.date').datepicker({
                    autoclose: true,
                    dateFormat: "dd-mm-yyyy"
                });
            </script>
        </form>
    @endguest
</div>
@endsection
