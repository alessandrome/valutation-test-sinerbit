@extends('layouts.base')

@section('content')
<div class="container">
    @foreach ($clients as $key=>$value)
    <div class="row">
        <div class="card panel panel-info">
            <a href="{{ URL::to('client/'.$value->id) }}">
                <div class="panel-heading">
                    <div class="col-md-10 col-offset-1">
                        <h4><strong> {{ $value->name.' '.$value->surname }} </strong></h4>
                    </div>
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-4 col-offset-1">
                            <strong>Email:</strong> {{ $value->email }}
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4 col-offset-1">
                            <strong>Tel. n°1</strong>: {{ $value->phone_n1 }}
                        </div>
                        <div class="col-md-4 col-offset-1">
                            <strong>Tel. n°2:</strong> {{ $value->phone_n2 }}
                        </div>
                    </div>
                    <div class="container"></div>
                </div>
            </a>
            <!-- Button to delete the client -->
            <form method="POST" action="{{ route('client.destroy', $value->id) }}">
                {{ method_field('DELETE') }}
                {{ csrf_field() }}
                <button type="submit" class="btn btn-primary floating-x">
                    Elimina
                </button>
            </form>
        </div>        
    </div>
    @endforeach
    
    <!-- Insert JQuery to answer if you want delete the client -->
</div>
<div class="container">
    <div class="col-md-12">
        <a href="{{ URL::to('client/create') }}" class="btn btn-info" >Register new Client</a>
    </div>
</div>
@endsection

