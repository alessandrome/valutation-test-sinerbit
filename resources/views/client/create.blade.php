@extends('layouts.base')

@section('content')
<div class="container">

    <!-- if there are creation errors, they will show here -->
    <!-- HTML::ul($errors->all()) not more well supported in laravel -->
    
    @guest
        <!-- Is not possible create something without to be a registered User -->
        <div class="panel panel-danger">
          <div class="panel-heading">Solo per utenti registrati!</div>
          <div class="panel-body">
            Se vuoi registrare un cliente devi prima registrarti o fare il login tu!
            <br/>
            <a href="{{ URL::to('login') }}" class="card btn btn-link">Login</a> <a href="{{ URL::to('register') }}" class="card btn btn-link">Register</a>
          </div>
        </div>
    
    @else
        @if(isset($errors) && count($errors)>0)
        <div class="row">
            <div class="alert alert-danger">
                The following forms are <strong>incorrect</strong>:
                <ul>
                @foreach($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
                </ul>
            </div>
        </div>
        @endif
        <form method="POST" action="/client">
            {{ csrf_field() }}
            <div class="row">
                <div class="col-md-4 col-offset-1 form-group">
                    <label name="name_label">Nome</label>
                    <input type="text" name="name"></input> 
                </div>
                <div class="col-md-4 col-offset-1 form-group">
                    <label name="surname_label">Congome</label>
                    <input type="text" name="surname"></input> 
                </div>
            </div>

            <div class="row">
            <div class="form-group">
                <label name="email_label">Email</label>
                <input type="email" name="email"></input> 
            </div>
            </div>
            
            <div class="row">
                <div class="col-md-4 col-offset-1 form-group">
                    <label name="phone_1_label">Numero °1</label>
                    <input type="text" name="phone_1"></input> 
                </div>
                <div class="col-md-4 col-offset-1 form-group">    
                    <label name="phone_2_label">Numero °2</label>
                    <input type="text" name="phone_2"></input> 
                </div>
            </div>
            
            <input type="hidden" name="worker_id" value="{{ Auth::user()->id }}"/>
            
            <input type="submit"/>

            <script>
            </script>
        </form>
    @endguest
    
</div>
@endsection
