@extends('layouts.base')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Editing {{ $client->name." ".$client->surname }} info</div>

                <div class="panel-body">
                    <form class="form-horizontal" method="POST" action="{{ route('client.update',$client->id) }}" required>
                        {{ method_field('PUT') }}
                        {{ csrf_field() }}

                        <!-- Name -->
                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label for="name" class="col-md-4 control-label">Nome</label>

                            <div class="col-md-6">
                                <input  type="text" class="form-control" name="name" value="{{ $client->name }}" required>

                                @if ($errors->has('name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        
                        <!-- Surname -->
                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label for="surname" class="col-md-4 control-label">Surname</label>

                            <div class="col-md-6">
                                <input  type="text" class="form-control" name="surname" value="{{ $client->surname }}">

                                @if ($errors->has('surname'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('surname') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        
                        <!-- Email input -->
                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email" class="col-md-4 control-label">E-Mail Address</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control" name="email" value="{{ $client->email }}" required autofocus>

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <!-- First Telephone number -->
                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label for="phone_1" class="col-md-4 control-label">Tel. n°1</label>

                            <div class="col-md-6">
                                <input type="text" class="form-control" name="phone_1" value="{{ $client->phone_n1 }}">

                                @if ($errors->has('phone_1'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('phone_1') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        
                        <!-- Second Telephone number -->
                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label for="phone_2" class="col-md-4 control-label">Tel. n°2</label>

                            <div class="col-md-6">
                                <input type="text" class="form-control" name="phone_2" value="{{ $client->phone_n2 }}">

                                @if ($errors->has('phone_2'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('phone_2') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-8 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    EDIT
                                </button>
                            </div>
                        </div>
                        
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

