@extends('layouts.base')

@section('content')
<div class="container">
    <h1>Showing <strong>{{ $client->name." ".$client->surname }}</strong></h1>

    <div class="jumbotron text-center">
        <h2>{{ $client->name." ".$client->surname }}</h2>
        <p>
            <strong>Email:</strong> {{ $client->email }}<br>
            <strong>Tel. n°1:</strong> {{ $client->phone_n1 }}<br/>
            <strong>Tel. n°2:</strong> {{ $client->phone_n2 }}<br/>
        </p>
        <a class="btn btn-primary " href="{{ route('client.edit',$client->id) }}">Edit client</a>
        <form method="POST" action="{{ route('client.destroy', $client->id) }}">
            {{ method_field('DELETE') }}
            {{ csrf_field() }}
            <button type="submit" class="btn btn-primary">
                Elimina
            </button>
        </form>
    </div>
</div>
@endsection

